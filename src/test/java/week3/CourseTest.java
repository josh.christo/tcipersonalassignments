package week3;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.time.LocalDate;

@RunWith(JUnitParamsRunner.class)
public class CourseTest {
//    private static final LocalDate startDate = LocalDate.parse("2020-09-15");
//    private static final LocalDate endDate = LocalDate.parse("2019-09-17");

//    @Test(expected = CourseDateException.class)
//    public void Constructor_InvalidStartAndEndDate_ExceptionThrown(LocalDate startDate, LocalDate endDate) throws CourseDateException {
//        Course course = new Course("Course Name", startDate, endDate);
//    }

    @Test(expected = CourseDateException.class)
    @Parameters(method = "generateInvalidStartAndEndDate")
    public void Constructor_InvalidStartAndEndDate_ExceptionThrown(LocalDate startDate, LocalDate endDate) throws CourseDateException {
        Course course = new Course("Course Name", startDate, endDate);
    }

    private static Object[] generateInvalidStartAndEndDate(){
        return new Object[]{
            new Object[]{LocalDate.parse("2020-09-15"), LocalDate.parse("2019-09-17")},
            new Object[]{LocalDate.parse("2020-09-15"), LocalDate.parse("2020-07-15")},
            new Object[]{LocalDate.parse("2020-09-15"), LocalDate.parse("2020-09-10")},
        };
    }

    @Test
    @Parameters(method = "generateValidStartAndEndDate")
    public void Constructor_ValidStartAndEndDate_ExceptionThrown(LocalDate startDate, LocalDate endDate) throws CourseDateException {
        Course course = new Course("Course Name", startDate, endDate);
    }

    private static Object[] generateValidStartAndEndDate(){
        return new Object[]{
                new Object[]{LocalDate.parse("2005-09-15"), LocalDate.parse("2006-09-17")},
                new Object[]{LocalDate.parse("2005-09-15"), LocalDate.parse("2020-10-15")},
                new Object[]{LocalDate.parse("2005-09-15"), LocalDate.parse("2005-09-17")},
        };
    }
}
