package week3;

import java.time.LocalDate;

public class Course {
    private String courseName;
    private LocalDate startDate;
    private LocalDate endDate;

    public Course(String courseName, LocalDate startDate, LocalDate endDate) throws CourseDateException {
        if(startDate.isAfter(endDate)) {
            throw new CourseDateException();
        }
        else{
            this.courseName = courseName;
            this.startDate = startDate;
            this.endDate = endDate;
        }
    }
}
