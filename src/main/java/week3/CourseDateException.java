package week3;

public class CourseDateException extends Exception {
    public CourseDateException() {
    }

    public CourseDateException(String message) {
        super(message);
    }
}
